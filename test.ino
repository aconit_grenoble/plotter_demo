

#include <math.h>


//Déclaration des pins
const int VY_Lissajous2=31;
const int VY_Lissajous1=29;
const int VY_SinAmorti=27;
const int VY_Triangle=25;
const int VY_Sinus=23;
const int Plume=45;
const int Depart=47;
const int VX_f_t=53;
const int VX_Triangle=51;
const int VX_Sinus=49;
const int Coeff[10]={41,39,37,35,33,32,34,36,38,40};
const int Phi=8;
//Voie X DAC0
//Voie Y DAC1





//Fonctions d'interruption
//l'interruption en mode coup par coup est prioritaire sur l'interuption en mode continu
/*
void interruptCoupParCoup(){
  digitalWrite(Plume,LOW);//Plume levée
  analogWrite(DAC0,0);//Positionnement a gauche de la feuille
  analogWrite(DAC1,2047);
  while (digitalRead(Depart)==HIGH){}
  digitalWrite(Plume,HIGH);//Plume baissée

}


void interruptContinu(){
  if (digitalRead(VX_f_t)==LOW)
    interruptCoupParCoup();
  else{
    digitalWrite(Plume,LOW);//Plume levée
    analogWrite(DAC0,2047);//Positionnement au centre de la feuille
    analogWrite(DAC1,2047);
    while (digitalRead(Depart)==HIGH){}
    digitalWrite(Plume,HIGH);//Plume baissée
    }


}
*/
//Fonctions mathématiques

float triangle(float x){
  return (1/M_PI)*acos(cos(x))*4095;
  }

float cosinus(float x) {
  return ((cos(x)+sin(x+M_PI/2))/2+1)*2047;
  }

float droite(float x,float  in_min,float in_max,float out_min, float out_max)
{
  return (x*(out_max-out_min)+out_min*in_max-out_max*in_min)/(in_max-in_min);
}

float cosam(float x,float coeff,float dephasage) {
  return (((cos(x-dephasage)+sin(x-dephasage+M_PI/2))*exp(-x*coeff))/2+1)*2047;                                                               //!!!!!!!!!
  }
  
float cosam2(float x,float coeff,float dephasage) {
  return ((cos(x-dephasage)+sin(x-dephasage+M_PI/2))*exp(-x*coeff))/2;                                                               //!!!!!!!!!
  }
  

  //Fonction permettant de "lire" le coefficient d'amortissement
float getcoeff(){
  int i=0;
  float coefficient;
  while(digitalRead(Coeff[i])==HIGH && i<=9){i++;}
  switch (i) {
    case 0:
      coefficient=0;
      break;
    case 1:
      coefficient=0.01;
      break;
    case 2:
      coefficient=0.02;
      break;
    case 3:
      coefficient=0.03;
      break;
    case 4:
      coefficient=0.05;
      break;
    case 5:
      coefficient=0.1;
      break;
    case 6:
      coefficient=0.2;
      break;
    case 7:
      coefficient=0.4;
      break;
    case 8:
      coefficient=0.7;
      break;
    case 9:
      coefficient=1;
      break;
    default:
      coefficient=1;
      break;
  }
  return coefficient;
}

void setup() {
 //initialisation des pins et activation de la resistance de pull up interne
 pinMode(VY_Lissajous2,INPUT_PULLUP);
  pinMode(VY_Lissajous1,INPUT_PULLUP);
  pinMode(VY_SinAmorti,INPUT_PULLUP);
  pinMode(VY_Triangle,INPUT_PULLUP);
  pinMode(VY_Sinus,INPUT_PULLUP);
  pinMode(Plume,OUTPUT);
  pinMode(43,OUTPUT);
  pinMode(Depart,INPUT_PULLUP);
  pinMode(VX_f_t,INPUT_PULLUP);
  pinMode(VX_Triangle,INPUT_PULLUP);
  pinMode(VX_Sinus,INPUT_PULLUP);
  
  
  
  digitalWrite(VY_Lissajous2,HIGH);
  digitalWrite(VY_Lissajous1,HIGH);
  digitalWrite(VY_SinAmorti,HIGH);
  digitalWrite(VY_Triangle,HIGH);
  digitalWrite(VY_Sinus,HIGH);
  digitalWrite(Depart,HIGH);
  digitalWrite(VX_f_t,HIGH);
  digitalWrite(VX_Triangle,HIGH);
  digitalWrite(VX_Sinus,HIGH);

  //idem pour le selecteur "coeff"
  for(int i=0;i<=9;i++){
  pinMode(Coeff[i],INPUT_PULLUP);
  digitalWrite(Coeff[i],HIGH);
  }

  //declaration des interruption sur chaque changement de position des selecteurs
/*  attachInterrupt(VY_Lissajous2,interruptContinu,FALLING);
  attachInterrupt(VY_Lissajous1,interruptContinu,FALLING);
  attachInterrupt(VY_SinAmorti,interruptContinu,FALLING);
  attachInterrupt(VY_Triangle,interruptContinu,FALLING);
  attachInterrupt(VY_Sinus,interruptContinu,FALLING);
  attachInterrupt(VX_f_t,interruptCoupParCoup,FALLING);
  attachInterrupt(VX_Triangle,interruptContinu,FALLING);
  attachInterrupt(VX_Sinus,interruptContinu,FALLING);*/

  //resolution lecture/ecriture analogique mise à 12 bits
  analogReadResolution(12); 
  analogWriteResolution(12);
  
 //interruptContinu();
 Serial.begin(9600);
}

void loop() {
  digitalWrite(Plume,HIGH);
  delay(100);
  digitalWrite(43,HIGH);
  delay(1000);
   digitalWrite(Plume,LOW);
   delay(100);
  digitalWrite(43,LOW);
  delay(1000);
  

  }

  // voieY=((cos(entree+dephasage)+sin(entree+dephasage+M_PI/2))/2+1)*2047;
  //if(entree>=2*M_PI){
    //entree=10;
  //}
  //angle=angle+0,001; 

    // entree+=0,01; 
 //   dephasage=map(analogRead(Phi),0,4095,0,M_PI);
  //   voieX=((cos(entree)+sin(entree+M_PI/2))/2+1)*2047;






//tests


/*
  for(i=0;i<=4095;i++){
    analogWrite(DAC1,i);
    delayMicroseconds(200);
 }

  for(i=4095;i>=0;i--){
    analogWrite(DAC1,i);
    delayMicroseconds(200);
 }

   for(entree=0;entree<=2*M_PI;entree+=0.001533){
    i=int(((cos(entree)+sin(entree+M_PI/2))/2+1)*2047);
    analogWrite(DAC1,i);
    analogWrite(DAC0,i);
    delayMicroseconds(5);
 }
}*/



/*
 while(1){
   if(angle>=2*M_PI){
     angle=0;
   }
   //dephasage=map(analogRead(Phi),0,4095,0,M_PI);
   voieX=((cos(angle)+sin(angle+M_PI/2))/2+1)*2047;
   voieY=((cos(angle)+sin(angle+M_PI/2))/2+1)*2047;
   
   analogWrite(DAC0,int(voieX));
   analogWrite(DAC1,int(voieY));
    
  angle+=0.001;  
  
  }



// Déclaration des variables
 float angle=0;
 float voieX=0;
 float voieY=0;
 float dephasage;
 float coefficient;
 float maximum;
 int i=0;
dephasage=droite(analogRead(Phi),0,4095,0,M_PI);
    coefficient=getcoeff();
    maximum=maxcosam(dephasage,coefficient);
    Serial.print("max :");
    Serial.print(maximum);
     Serial.print("   coeff :");
    Serial.print(coefficient);
     
  delay(100);

    Serial.print('\n');
*/


